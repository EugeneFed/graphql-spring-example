package com.spring_qraphql.model;

/**
 * Created by Evgeniy.Fedoseev on 3/15/2017.
 */
public class Hero {
    private String id;
    private String name;

    public Hero(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Hero() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
