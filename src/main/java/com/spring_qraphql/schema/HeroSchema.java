package com.spring_qraphql.schema;

import com.spring_qraphql.model.Hero;
import graphql.Scalars;
import graphql.relay.Relay;
import graphql.relay.SimpleListConnection;
import graphql.schema.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static graphql.Scalars.GraphQLID;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLObjectType.newObject;
import static graphql.schema.GraphQLSchema.newSchema;

/**
 * Created by Evgeniy.Fedoseev on 3/15/2017.
 */
public class HeroSchema {
    private GraphQLSchema schema;

    private GraphQLObjectType heroType;

    private List<Hero> heroes;

    private Hero theOnlyHero = new Hero();

    private Relay relay = new Relay();

    private GraphQLInterfaceType nodeInterface;

    private SimpleListConnection simpleConnection;

    public HeroSchema() {
        fillHeroes();
        createSchema();
    }

    private void createSchema() {
        TypeResolverProxy typeResolverProxy = new TypeResolverProxy();
        nodeInterface = relay.nodeInterface(typeResolverProxy);
        simpleConnection = new SimpleListConnection(heroes);

        createHeroType();

        typeResolverProxy.setTypeResolver(object -> {
            if (object instanceof Hero) {
                return heroType;
            }

            return null;
        });


//        DataFetcher heroDataFetcher = environment -> {
//            String id = environment.getArgument("id");
//            return new Hero();
//        };
//
//        GraphQLObjectType QueryRoot = newObject()
//                .name("Root")
//                .field(newFieldDefinition()
//                        .name("hero")
//                        .type(heroType)
//                        .staticValue(heroes)
//                        .build())
//                .field(relay.nodeField(nodeInterface, heroDataFetcher))
//                .build();

        GraphQLObjectType heroesQuery = newObject()
                .name("heroesQuery")
                .field(newFieldDefinition()
                        .name("hero")
                        .type(new GraphQLList(heroType))
                        .dataFetcher(environment -> heroes)
                        .build())
                .build();

        schema = newSchema()
                .query(heroesQuery)
                .build();
    }

    private void createHeroType() {
        heroType = newObject()
                .name("hero")
                .field(newFieldDefinition()
                        .name("id")
                        .type(new GraphQLNonNull(GraphQLID))
                        .build())
                .field(newFieldDefinition()
                        .name("name")
                        .type(Scalars.GraphQLString)
                        .build())
                .build();
    }

    public GraphQLSchema getSchema() {
        return schema;
    }

    private void fillHeroes() {
        heroes = new ArrayList<>(Arrays.asList(
                new Hero("11", "Mr. Nice"),
                new Hero("12", "Narco"),
                new Hero("13", "Bombasto"),
                new Hero("14", "Celeritas"),
                new Hero("15", "Magneta"),
                new Hero("16", "RubberMan"),
                new Hero("17", "Dynama"),
                new Hero("18", "Dr IQ"),
                new Hero("19", "Magma"),
                new Hero("20", "Tornado")
        ));
    }
}
