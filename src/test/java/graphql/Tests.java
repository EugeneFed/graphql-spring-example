package graphql;

import com.spring_qraphql.schema.HeroSchema;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

/**
 * Created by Evgeniy.Fedoseev on 3/16/2017.
 */
public class Tests {
    private HeroSchema schema = new HeroSchema();
    private GraphQL graphql = new GraphQL(schema.getSchema());

    @Test
    public void testGetHeroes() {
        String query = "{hero {name}}";
        ExecutionResult executionResult = graphql.execute(query);

        if (executionResult.getErrors().size() > 0) {
            System.err.printf("Errors: %s", executionResult.getErrors());
        }

        System.out.println(executionResult);
    }
}
